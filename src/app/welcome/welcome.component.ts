import { LoginComponent } from './../login/login.component';
import { User } from './../interfaces/user';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit 
{
  email:string;
  user:Observable<User | null>
  userId:string;

   constructor(public afAuth:AngularFireAuth, 
              public router: Router,
              public auth:AuthService) 
              {
                this.user = this.afAuth.authState;
                console.log('User Observable: ',this.user)
                
              }
  

  ngOnInit() 
  {
    this.auth.user.subscribe(
      user => 
      {
        this.userId = user.uid;
        this.email = user.email;
      }
    )}

}
