import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userEmail: string= "a@a.com"
  constructor(private auth:AuthService) { }

  ngOnInit() {
    //this.userEmail = this.auth.getUser2().email;
  }
}
