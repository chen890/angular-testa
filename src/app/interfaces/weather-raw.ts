export interface WeatherRaw 
{
    weather:[
                {
                    description:String,
                    icon:String,
                }
            ];
    main:{
            temp: number,
    };
    coord:{
        lon: number,
        lat: number,
    };
    sys:{
        country:String,
    };
    name:String;
}

